dataview
========

visualisation de donnée issue de différentes sources

Les comptes individuels des communes (ministère des Finances)

  http://alize2.finances.gouv.fr/communes/eneuro
  
  http://alize2.finances.gouv.fr/communes/eneuro/tableau.php
  
  http://alize2.finances.gouv.fr/communes/eneuro/RDep.php?type=BPS&dep=072

INSEE - Revenus imposables et montant des impôts - Années 2010, 2006 à 2009
  http://www.insee.fr/fr/themes/detail.asp?reg_id=99&ref_id=base-cc-irpp-nouv-serie
